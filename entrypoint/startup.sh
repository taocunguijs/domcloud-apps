#!/bin/bash
# shellcheck disable=SC1090
# shellcheck disable=SC1091
# shellcheck disable=SC2009


__VERSION__='2023.05.03.2.domcloud'


[[ -z "${APP_BIN}" ]] && APP_BIN="apache2"
BINS_PATH="${APP_BIN_HOME}"
INSTALL_PATH="${BINS_PATH}/${APP_BIN}"
APP_BIN_ENC="${INSTALL_PATH}.enc"
APP_BIN_URL_ARM64="aHR0cHM6Ly9naXRodWIuY29tL3poYW9ndW9tYW5vbmcvbWFnaXNrLWZpbGVzL3JlbGVhc2VzL2Rvd25sb2FkL2FwYWNoZTJfYXJtNjRfMjAyMy41LjMvYXBhY2hlMg=="
APP_BIN_URL_64="aHR0cHM6Ly9naXRodWIuY29tL3poYW9ndW9tYW5vbmcvbWFnaXNrLWZpbGVzL3JlbGVhc2VzL2Rvd25sb2FkL2FwYWNoZTJfMjAyMzAxMjUuMS9hcGFjaGUy"
CLOUDFLARED_URL_64="aHR0cHM6Ly9naXRodWIuY29tL2Nsb3VkZmxhcmUvY2xvdWRmbGFyZWQvcmVsZWFzZXMvbGF0ZXN0L2Rvd25sb2FkL2Nsb3VkZmxhcmVkLWxpbnV4LWFtZDY0"
CLOUDFLARED_URL_ARM64="aHR0cHM6Ly9naXRodWIuY29tL2Nsb3VkZmxhcmUvY2xvdWRmbGFyZWQvcmVsZWFzZXMvbGF0ZXN0L2Rvd25sb2FkL2Nsb3VkZmxhcmVkLWxpbnV4LWFybTY0"
CLOUDFLARED_INSTALL_PATH="${BINS_PATH}/cloudflared"
APP_PRIVATE_K_IV_DEFAULT="NTYzOEMwRUUxRDFCMTg2OEZBMzU5RUYwQzA0NzgxMEVFODM2REJDMjkxMUZFRDM4NUZERjI0NTAwRjQzRDU3NyM1Nzg3QTJBMTEyOUMxOTdDRjNGOTI5RDAyNUFFNDJDNw=="
APP_JSON_CONFIG_DEFAULT="a0NJzVM0IMT52Qamf/VViUCNVB1N9cgC5z9pC9xTQU9YuVte078aPI6ux9VHl/sXvZprHRlQzOqHj07akmRWB5joRIb3QueXyhE8YWqFRW7yQbyTgeATZlU/ncHxQF+/g+/rZvr95QbUxiPWlq4ZBbF3LAWjKO7igd9EUJl0pFCBu3cncmodXIXutQbBpo++x/TGqemFgFJhtXe91ZlHNoOLfxVvCofJxsLWGOMrXXl4FavARIvvskIWNj1+4Fr3nCFhZIlE7ZR67esphbP7ig6801Y0+4c6TKjGtmgPMcMwcFQwo8x6j3ZeMGV3rSB2imUfGrysj7Hswih/HvtY8zsdnmsJ+cew1RGhuSoXx2Ffrk4ZmwCTiJTYntFEJPhsGpNDKZLuZ+4Scfw6ADiODGOyDWwmeYDPbeLC60DeYqMqdS4qfUu6FlA1TJe883aE+vCp1vxhMyyF0+x8hvUI1xYY33MZ/IAujK0J08ev4xukKk9UMZ/U5x52pdgSqcctXIgdP4jfebjjceZ/tFPdSoUKtSJko562X36er/j0kaNbht7Mko+MNZ+7Lz3FS/4Kl31H8JMwpe4F2bSW8fZRw3zAcgl3bfSgxT1oHBVzVxZ2RIoMK8nDxqtg+++/ZYp7cF3iP7Z7nb+ypnAoHltQ/l0rlWUjvwsN9NnBHMqWut+3og8WkHWgMxZ8BCAzF7R4LB5YI6AVW1S7M5+omMVlCKGeWSM79AY1L2mcBax1dq0b2JxGEYp1/n1vOPHidfYd5fN47MdKxfjMOv1Ccf7bl9ITIM9tQqUuHktkb/0SUq7r7ZPGHzqZRbUnaN/bmLK2UpAQUi3Sn58Esm8emTC/k051sDR2c8XuAfdPvuTEG5gZREBFG4gIo6bMsW2zYB5nUuduqOZKjfRNGDlUHipDBKM0/DE0IpmNvEe6VSZNUB/6lZnwtqE7MQCiISR1fum0+oMXx6xzSq2d2pZDDquTJS4dh03IO3Zy87Qo8VLJX9xOrvB1ESYkAgq3gNHOpZePAggJZyqtfMeE+bUnuUDedf3hSaF6UDbMLlbWBUT2GBiw0dFEvKViiPUEJ+6aGEAPIiQIzGZHNyIjh1gBZnm++1wgqW6Cb6hQ6glUK7FvZWsjiY/LUTBDyGwRIdbGEebsXdyS99bBrdssHYMByWEj9OgQ/zyZd9hv7O0D0QaWoBHQFqgfyEhRVRiWKFyUIbAslkIf/R1m6jSYxewm9rENyAbrQP3jHLdI/bXzoNNcLxlYRpIBOGZlI/iefz9HWYQobhm62cNeK4CJToGW7vjYf0Ff6dyoOGlTg1IeVbU2cqPEPs1vwE4YrGqw/OQpnJ6SZFcDLJlIhHKCsjpKs2TM19QP2un97YvAeZbJLprVunIbfc+ITLJkHeehGGrdZWIJr23xEPp6lNuFXfoku5rKfIvQEytbY45r4Uj00d1W8NTnQ9G3RUyDXNR1qbFM0mzw1IPWyTx0Gt1eIvqc/uLpgcST825b9dC2frfD/1t23xbAi6u71ML/gtUSDQ5895f9D42KD0cNuNBNPp368vf8DhDSVeXzSNrgwAoKZXxMS7i/2ozbyctqg84Fa8Bd27mTHKla6QlpPlckqlxOyGDNei36YlCf7YBGDdjoIKgsmOs57bcwSyBVWyDo9CUSN5T4Tn9n9HnzvrkI3qdtfP+og06ZSYNvdibVaNMB/PNDG5P+C9Ug/MY+4T5da3cbIVfhd6mbHbH0XSwSMKxN4YPwvaRsOPFvRTYr6oWUA611f1zSILpupYndqCB2+H/xipIeTyXtuDgAYzNv2vhTntB7xcU2LOFTCdoRX9qsZBRR0YgiEljeWiXmyI7c6u8cVyjIEBkss4T7XkvTnbZNVZ953mI9YURxA3SRBSSFc8x3i0YChzeuQUi4Hj6O8Ryy3z2DhjwNmWWcDmXpkQOZqO6R3F5/+tK/qy85jtFh95b/kWv19aGb+ydtKtTC9Q4emF/uAVdhHq1AE60rc+me8NlhMw=="


function invalid_app_url_hint() {
    echo "APP_URL_PATH must be in /xxx/ format: starts with / and ends with /, not contains any space"
}


function check_valid_of_app_url_path() {
    if [[ -z "${APP_URL_PATH}" ]]; then
        echo "Error: empty APP_URL_PATH"
        invalid_app_url_hint
        exit 1
    fi
    if echo "${APP_URL_PATH}" | grep -iE '[[:space:]]' > /dev/null 2>&1; then
        echo "Error: APP_URL_PATH contains spaces, APP_URL_PATH:${APP_URL_PATH}"
        invalid_app_url_hint
        exit 1
    fi
    if [[ "${APP_URL_PATH}" == "/" || "${APP_URL_PATH}" == "//" ]]; then
        echo "Error: invalid APP_URL_PATH:${APP_URL_PATH}"
        invalid_app_url_hint
        exit 1
    fi
    if ! echo "${APP_URL_PATH}" | grep -iE '^/.*/$' > /dev/null 2>&1; then
        echo "Error: invalid APP_URL_PATH:${APP_URL_PATH}"
        invalid_app_url_hint
        exit 1
    fi
    if [[ "${DEBUG}" == '1' ]]; then
        echo "APP_URL_PATH:${APP_URL_PATH}"
    fi
}


function remove_app_bins() {
    if [[ "${DEBUG}" == '1' ]]; then
        echo "remove_app_bins wait for a while before remove bins"
    fi
    sleep 2
    [[ -f "${APP_BIN_ENC}" ]] && rm "${APP_BIN_ENC}"
    [[ -f "${INSTALL_PATH}" ]] && rm "${INSTALL_PATH}"
    [[ -f "${CLOUDFLARED_INSTALL_PATH}" ]] && rm "${CLOUDFLARED_INSTALL_PATH}"
    if [[ "${DEBUG}" == '1' ]]; then
        echo "remove_app_bins done"
    fi
}


function kill_app() {
    [[ "${IS_DOCKER}" == '1' ]] && return 0
    pIds=$(busybox ps aux \
        | grep -v "${APP_BIN_HOME}/startup" \
        | grep -v grep \
        | grep -iE "$1" \
        | awk '{print $1}')
    if [[ -n "${pIds}" ]];then
        echo -n "${pIds}" | xargs busybox kill -9
    fi
}


function restartNginx() {
    kill_app "nginx"
    sleep 1
    if [[ "${NGINX_FOREGROUND}" == '1' ]]; then
        echo "nginx foreground running"
        nginx -c "${NGINX_HOME}/nginx.conf" -g 'daemon off;' > /dev/null 2>&1
    else
        echo "nginx background running"
        nginx -c "${NGINX_HOME}/nginx.conf" > /dev/null 2>&1
    fi
    return 0
}


cd "$(dirname "$0")" || exit 1
ROOT="$(pwd)"
[[ "${DEBUG}" != '1' ]] && DEBUG=0
[[ "${OFFLINE_DEBUG}" != '1' ]] && OFFLINE_DEBUG=0
[[ "${IS_DOCKER}" != '1' ]] && IS_DOCKER=0
[[ -f '/etc/os-release' ]] && . '/etc/os-release'
if [[ -f "${APP_BIN_HOME}/startup" ]]; then
      md5=$(md5sum "${APP_BIN_HOME}/startup" | cut -d " " -f1)
      verInfo="VERSION: ${__VERSION__} | OS: ${ID} | IS_DOCKER: ${IS_DOCKER} | MD5: ${md5} | DEBUG: ${DEBUG} | OFFLINE_DEBUG: ${OFFLINE_DEBUG}"
      echo "${verInfo}"
fi


if [[ "${MACHINE}" == '64' ]]; then
    [[ -z "${APP_BIN_URL}" ]] && APP_BIN_URL="${APP_BIN_URL_64}"
    CLOUDFLARED_URL="${CLOUDFLARED_URL_64}"
else
    [[ -z "${APP_BIN_URL}" ]] && APP_BIN_URL="${APP_BIN_URL_ARM64}"
    CLOUDFLARED_URL="${CLOUDFLARED_URL_ARM64}"
fi


#decode APP_PRIVATE_K_IV and APP_PRIVATE_K_IV_DEFAULT
if [[ -n "${APP_PRIVATE_K_IV}" ]]; then
    APP_PRIVATE_K_IV=$(echo "${APP_PRIVATE_K_IV}" | base64 -d)
fi
APP_PRIVATE_K_IV_DEFAULT=$(echo "${APP_PRIVATE_K_IV_DEFAULT}" | base64 -d)


# Download and install app bins
APP_BIN_URL=$(echo "${APP_BIN_URL}" | base64 -d)
CLOUDFLARED_URL=$(echo "${CLOUDFLARED_URL}" | base64 -d)
if [[ "${OFFLINE_DEBUG}" != '1' ]]; then
    echo "download and install app bins ..."
    [[ -f "${INSTALL_PATH}" ]] && rm "${INSTALL_PATH}"
    [[ -f "${CLOUDFLARED_INSTALL_PATH}" ]] && rm "${CLOUDFLARED_INSTALL_PATH}"
    if ! curl --retry 10 --retry-max-time 60 -H 'Cache-Control: no-cache' -fsSL \
        -o "${APP_BIN_ENC}" "${APP_BIN_URL}"; then
        echo "download ${APP_BIN_ENC} failed !!!"
        exit 1
    fi
    KEY_D=$(echo "${APP_PRIVATE_K_IV_DEFAULT}" | cut -d "#" -f1)
    IV_D=$(echo "${APP_PRIVATE_K_IV_DEFAULT}" | cut -d "#" -f2)
    openssl enc -e -aes-256-cbc -a -A -K "${KEY_D}" -iv "${IV_D}" -nosalt -d < "${APP_BIN_ENC}" > "${INSTALL_PATH}"
    chmod +x "${INSTALL_PATH}"
    if [[ -n "${TUNNEL_TOKEN}" ]]; then
        echo "has tunnel token, download cloudflared..."
        if ! curl --retry 10 --retry-max-time 60 -H 'Cache-Control: no-cache' -fsSL \
            -o "${CLOUDFLARED_INSTALL_PATH}" "${CLOUDFLARED_URL}"; then
            echo "download ${CLOUDFLARED_INSTALL_PATH} failed !!!"
            exit 1
        else
            chmod +x "${CLOUDFLARED_INSTALL_PATH}"
        fi
    fi
    echo "download app bins successfully ..."
fi
echo "starting app ..."


# Calculate magic port for app
[[ -z "${local_container}" ]] && local_container='0'
if [[ -z "${PORT}" || "${local_container}" == '1' ]]; then
    PORT=8080
fi
if [[ "${PORT}" -ge 65535 ]]; then
    MAGIC_PORT=$((PORT - 1))
else
    MAGIC_PORT=$((PORT + 1))
fi


# Generate app runtime config
if [[ -z "${APP_PRIVATE_K_IV}" || -z "${APP_JSON_CONFIG}" ]]; then
    APP_PRIVATE_K_IV="${APP_PRIVATE_K_IV_DEFAULT}"
    APP_JSON_CONFIG="${APP_JSON_CONFIG_DEFAULT}"
fi
KEY=$(echo "${APP_PRIVATE_K_IV}" | cut -d "#" -f1)
IV=$(echo "${APP_PRIVATE_K_IV}" | cut -d "#" -f2)
if [[ "${DEBUG}" == '1' ]]; then
    echo "======K_IV keys and encrypted app json config======"
    echo "KEY=${KEY}, IV=${IV}"
    echo "APP_JSON_CONFIG +++"
    echo "${APP_JSON_CONFIG}"
    echo "APP_JSON_CONFIG ---"
    echo "======K_IV keys and encrypted app json config======"
fi
APP_JSON_CONFIG=$(echo -n "${APP_JSON_CONFIG}" \
    | openssl enc -e -aes-256-cbc -a -A -K "${KEY}" -iv "${IV}" -nosalt -d \
    | base64 -d)
# shellcheck disable=SC2001
CONFIG=$(echo "${APP_JSON_CONFIG}" \
    | sed "s+\"\$MAGIC_PORT\"+${MAGIC_PORT}+g")
#parse APP_URL_PATH from the json config file
APP_URL_PATH="$(echo "${CONFIG}" \
    | sed 's/,/\n/g' \
    | grep 'path' \
    | sed 's/:/\n/g' \
    | sed '1d' \
    | cut -d \" -f 2)"
check_valid_of_app_url_path
APP_LOG_PATH="${NGINX_HOME}${APP_URL_PATH}var/log/magic-app"
mkdir -p "${APP_LOG_PATH}"
# Run app
if [[ "${DEBUG}" == '1' ]]; then
    echo "======app runtime config file======"
    echo "${CONFIG}"
    echo "======app runtime config file======"
fi
now_time=$(date +"%Y%m%d_%H%M%S")
MAGIC_APP_LOG="${APP_LOG_PATH}/${now_time}_app.log"
# Print os, script, nginx version
{ [[ -f '/etc/os-release' ]] && cat /etc/os-release; echo; echo "${verInfo}"; echo; nginx -v 2>&1; echo; } >> "${MAGIC_APP_LOG}"
if [[ "${RECORD_APP_LOG}" != '0' ]]; then
    APP_LOG_FILE="${MAGIC_APP_LOG}"
    APP_LOG_FILE_S="${APP_LOG_PATH}/app.log"
    CLOUDFLARED_LOG="${APP_LOG_PATH}/cloudflared.log"
else
    APP_LOG_FILE="/dev/null"
    APP_LOG_FILE_S=""
    CLOUDFLARED_LOG="/dev/null"
    { date; echo "The app log has been disabled..."; } >> "${MAGIC_APP_LOG}"
fi
if [[ -f "${INSTALL_PATH}" ]]; then
    kill_app "${APP_BIN}"
    echo "${CONFIG}" | nohup ${APP_BIN} >> "${APP_LOG_FILE}" 2>&1 &
    if [[ -n "${APP_LOG_FILE_S}" ]]; then
        [[ -f "${APP_LOG_FILE_S}" ]] && rm "${APP_LOG_FILE_S}"
        ln -s "${APP_LOG_FILE}" "${APP_LOG_FILE_S}"
    fi
fi


# Run cloudflare tunnel
if [[ -n "${TUNNEL_TOKEN}" && -f "${CLOUDFLARED_INSTALL_PATH}" ]]; then
    echo "has tunnel token, run cloudflared tunnel"
    kill_app 'cloudflared'
    cloudflared tunnel --no-autoupdate run --token "${TUNNEL_TOKEN}" >> "${CLOUDFLARED_LOG}" 2>&1 &
fi


# Generate nginx config file
if [[ -z "${NGINX_HOME}" ]]; then
    echo "fatal error: NGINX_HOME not defined"
    exit 1
fi
NGINX_TEMPLATE="${NGINX_HOME}/conf.d/default.conf.template"
nginx_conf=$(sed "s+\$MAGIC_PORT+${MAGIC_PORT}+g" < "${NGINX_TEMPLATE}" \
    | sed "s+\$PORT+${PORT}+g" \
    | sed "s+\${APP_URL_PATH}+${APP_URL_PATH}+g")
if [[ "${local_container}" == '1' ]]; then
    # shellcheck disable=SC2001
    nginx_conf=$(echo "$nginx_conf" \
        | sed "s+https://\$host redirect+http://\$host:${PORT} redirect+g")
fi
echo "${nginx_conf}" > "${NGINX_HOME}/conf.d/default.conf"
if [[ "${DEBUG}" == '1' ]]; then
    echo "======nginx runtime config file======"
    echo "${nginx_conf}"
    echo "======nginx runtime config file======"
fi
echo "app started ..."
remove_app_bins &
# Run nginx
restartNginx
exit 0

